# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

admindata = {
    email: 'ichmagfisch@googlemail.com',
    password: 'adminPass123!',
    password_confirmation: 'adminPass123!'
}


admin = Admin.new(admindata)

unless Admin.where(email: admin.email).exists?
  admin.save!
end
