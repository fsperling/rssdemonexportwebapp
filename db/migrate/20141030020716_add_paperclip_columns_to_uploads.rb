class AddPaperclipColumnsToUploads < ActiveRecord::Migration
  def change
    add_column :uploads, :database_file_name, :string
    add_column :uploads, :database_content_type, :string
    add_column :uploads, :database_updated_at, :datetime
  end
end
