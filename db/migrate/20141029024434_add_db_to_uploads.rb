class AddDbToUploads < ActiveRecord::Migration
  def change
    add_column :uploads, :database_file, :binary
  end
end
