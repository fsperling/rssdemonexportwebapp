require 'uploads_helper'
require 'addressable/uri'

class UploadsController < ApplicationController
  rescue_from ActionController::ParameterMissing, with: :empty_file_select

  def new
    @upload = Upload.new
  end

  def create
    @upload = Upload.create(user_params)
     if @upload.save
        redirect_to @upload#, notice: 'upload was successfully created.'
     else
	render 'new'
     end
  end

  def show
    @upload = Upload.find(params[:id])
    uri = Addressable::URI.parse @upload.database.url
    ahoy.track("try export", {"uri" => uri.path})
    @exportfile = UploadsHelper.runexport(uri.path, ahoy)
  end

  def yes
    ahoy.track("sucessful export")
    render text: 'Feedback received', content_type: 'text/plain'
  end

  def no
    ahoy.track("failed export")
    render text: 'Neg Feedback received', content_type: 'text/plain'
  end

  def user_params
    params.require(:upload).permit(:database)
  end

  private

  def empty_file_select
    @upload = Upload.new
    render 'new'
  end
end
