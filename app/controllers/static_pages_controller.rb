require 'upload'

class StaticPagesController < ApplicationController
  def home
    @upload = Upload.new
  end

  def stats
    @visits = Visit.all
    @events = Ahoy::Event.all
    @event_counts = Ahoy::Event.group(:name).count
    @exports = Ahoy::Event.where(name: "export complete").map(&:properties)
  end
end
