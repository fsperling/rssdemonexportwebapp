class Upload < ActiveRecord::Base
	has_attached_file :database
	#do_not_validate_attachment_file_type :database
	validates_attachment_content_type :database, :content_type => ["application/octet-stream"]
	validates_attachment_file_name :database, :matches => [/db\Z/]
	
end
