class Podcast
  attr_reader :title, :htmlurl, :url, :type

  def initialize title, htmlurl, url, type
    @title = title
    @htmlurl = htmlurl
    @url = url
    @type = type
  end
end


